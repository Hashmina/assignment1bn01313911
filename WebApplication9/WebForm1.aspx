﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication9.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Music Event!!</title>
</head>
<body>
    <form id="form1" runat="server">
        <div runat="server" ID="res">
         
            <h3 runat="server" id ="info"></h3>
            <p>Register for Music Event</p>
            <p>^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^</p>
            <p>       Welcome to Jazz Night 2018!</p>
            <p>^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^</p>

            <label for="GuestName">Name: </label>
            <asp:TextBox runat="server" ID="GuestName" placeholder="Name" > </asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Name:" ControlToValidate="GuestName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <br/>
            <label for="GuesEmail"> Email address: </label>
            <asp:TextBox runat="server" ID="GuestEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your Email address" ControlToValidate="GuestEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="GuestEmail" ErrorMessage="You've entered Invalid Email"></asp:RegularExpressionValidator>
            <br/>
            <br/>
            <label for="GuestPhone">Phone Number: </label>
            <asp:TextBox runat="server" ID="GuestPhone" placeholder="Phone"> </asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Phone Number:" ControlToValidate="GuestPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="GuestPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="You've entred invalid phone number"></asp:CompareValidator>
            <br />
            <br />
            <label for="GuestAdress">Address: </label>
            <asp:TextBox runat="server" ID="GuestAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="GuestAddress" ErrorMessage="Enter your address"></asp:RequiredFieldValidator>
            <br />
            <br/>
            <label for="ageselect">Age group: </label>
            <asp:DropDownList runat="server" ID="ageselect">
                <asp:ListItem Value="18-25" Text="18-25"></asp:ListItem>
                <asp:ListItem Value="26-35" Text="26-35"></asp:ListItem>
                <asp:ListItem Value="36-55" Text="36-55"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br/>
            
            
            <label for="GuestCount">Total number of guests: </label>
              <asp:RadioButtonList ID="GuestCount" runat="server"  >
                <asp:ListItem Text="1 Adult" Value="1" />
                 <asp:ListItem Text="2 Adults" Value="2" />
                  <asp:ListItem Text="3 Adults" Value="3" />
                  <asp:ListItem Text="4 Adults" Value="4" />
               </asp:RadioButtonList>
            
           
            <br/>
            <br />
            <label for="RowSelect">Select Row: </label>
            <asp:DropDownList runat="server" ID="RowSelect">
                <asp:ListItem Value="r1" Text="A"></asp:ListItem>
                <asp:ListItem Value="r2" Text="B"></asp:ListItem>
                <asp:ListItem Value="r3" Text="C"></asp:ListItem>
                <asp:ListItem Value="r4" Text="D"></asp:ListItem>
                <asp:ListItem Value="r5" Text="E"></asp:ListItem>
                <asp:ListItem Value="r6" Text="F"></asp:ListItem>
                <asp:ListItem Value="r7" Text="G"></asp:ListItem>
                <asp:ListItem Value="r8" Text="H"></asp:ListItem>
                <asp:ListItem Value="r9" Text="I"></asp:ListItem>
                <asp:ListItem Value="r10" Text="J"></asp:ListItem>
            </asp:DropDownList>

            <br/>
            <br/>

            <label for="SeatSelect">Select seat/s :</label>
            <div id="available_seats" runat="server">
            <asp:CheckBox ID="CheckBox1" runat="server" Text="1" />
            <asp:CheckBox ID="CheckBox2" runat="server" Text="2" />
            <asp:CheckBox ID="CheckBox3" runat="server" Text="3" />
            <asp:CheckBox ID="CheckBox4" runat="server" Text="4" />
            <asp:CheckBox ID="CheckBox5" runat="server" Text="5" />
            <asp:CheckBox ID="CheckBox6" runat="server" Text="6" />
            <asp:CheckBox ID="CheckBox7" runat="server" Text="7" />
            <asp:CheckBox ID="CheckBox8" runat="server" Text="8" />
            <asp:CheckBox ID="CheckBox9" runat="server" Text="9" />
            <asp:CheckBox ID="CheckBox10" runat="server" Text="10" />
            </div>
         
            <br />
            <br/>
            <label for="CardSelect">Enter your paymet option: </label>
            <asp:DropDownList runat="server" ID="CardSelect">
                <asp:ListItem Value="r1" Text="Credit Card"></asp:ListItem>
                <asp:ListItem Value="r2" Text="Debit Card"></asp:ListItem>
                <asp:ListItem Value="r3" Text="Net Banking"></asp:ListItem>
                
            </asp:DropDownList>
            <br />
            <br/>
            <asp:Button runat="server" ID="myButton" Text="Submit"/>
            <br />

            </div>
            <div runat="server" id="CustomerRes">

            </div>
            <div runat="server" id="GuestRes">

            </div>
            <div runat="server" id="EventRes">

            </div>
             <div runat="server" id="RegisterRes">

            </div>

            <footer runat="server" id="footer">


            </footer>
        </div>


            

        </div>
    </form>

</body>
</html>
