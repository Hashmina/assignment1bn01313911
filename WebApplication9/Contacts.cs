﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication9
{
    public class Contacts
    {
        public string Email;
        public string Phone;
        public int Adress;

        public Contacts(string em, string ph, int ad)
        {
            Email = em;
            Phone = ph;
            Adress = ad;
        }

        public string GuestName
        {
            get { return GuestName; }
            set { GuestName = value; }
        }
        public string GuestPhone
        {
            get { return GuestPhone; }
            set { GuestPhone = value; }
        }
        public string GuestEmail
        {
            get { return GuestEmail; }
            set { GuestEmail = value; }
        }
    }
}